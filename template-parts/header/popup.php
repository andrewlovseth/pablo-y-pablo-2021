<?php

    $slug = bearsmith_get_location($post);
    $text = get_field($slug . '_popup_text', 'options');
    $link = get_field($slug . '_popup_link', 'options');

    if($text):

?>

    <div class="popup">
        <?php if($link): ?>
            <a href="<?php echo $link; ?>" class="popup-link" rel="external"><span><?php echo $text; ?></span></a>
        <?php else: ?>
            <span class="popup-link" rel="external"><span><?php echo $text; ?></span></span>
        <?php endif; ?>
    </div>
	
<?php endif; ?>