<?php

    $slug = bearsmith_get_location($post);
    $location_header = get_field($slug . '_location_header', 'options');
    $location = get_field($slug . '_location', 'options');

?>

<div class="info left">
	<?php if($location_header): ?>
		<h3><?php echo $location_header; ?></h3>
	<?php else: ?>
		<h3>Location</h3>
	<?php endif; ?>

    <p><?php echo $location; ?></p>	
</div>