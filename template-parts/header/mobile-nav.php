<?php $slug = bearsmith_get_location($post); if(have_rows($slug . '_navigation','options')): ?>
	<nav class="mobile-nav">
        <ul>
            <?php while(have_rows($slug . '_navigation', 'options')): the_row(); ?>

				<?php if(get_sub_field('dropdown')): ?>

					<?php get_template_part('template-parts/header/site-nav-dropdown'); ?>

				<?php else: ?>

					<?php get_template_part('template-parts/header/site-nav-link'); ?>

				<?php endif; ?>
                
    		<?php endwhile; ?>
        </ul>
	</nav>
<?php endif; ?>