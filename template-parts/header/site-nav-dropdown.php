<?php 

    $main_link = get_sub_field('link');
    $dropdown = get_sub_field('dropdown');

    if( $main_link ): 
    $main_link_title = $main_link['title'];
?>

    <li class="site-nav__target">
        <span class="site-nav__target-link">
            <?php echo esc_html($main_link_title); ?>
			<?php get_template_part('template-parts/svg/down-arrow'); ?>
        </span>

        <ul class="site-nav__dropdown">
            <?php if(have_rows('menu_subnav', $dropdown->ID)): while(have_rows('menu_subnav', $dropdown->ID)): the_row(); ?>
            
            <?php 
                $link = get_sub_field('link');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_page = url_to_postid( $link_url );
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>
                <?php if(get_sub_field('show') == TRUE): ?>
                    <li class="site-nav__dropdown-item">
                        <a class="site-nav__dropdown-link" href="<?php echo esc_url($link_url); ?>" target="blank">
                            <?php echo esc_html($link_title); ?>
                        </a>
                    </li>
                <?php endif; ?>
                <?php endif; ?>
            <?php endwhile; endif; ?>

        </ul>
    </li>

<?php endif; ?>