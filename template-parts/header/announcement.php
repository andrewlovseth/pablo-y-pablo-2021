<?php

$slug = bearsmith_get_location($post);
$location = get_page_by_path($slug);
$show = get_field('show_announcement', $location->ID);
$announcement = get_field('announcement', $location->ID);

if(get_field('announcement_background_color', $location->ID)) {
	$background_color = get_field('announcement_background_color', $location->ID);
} else {
	$background_color = $black;
}

if(get_field('announcement_text_color', $location->ID)) {
	$text_color = get_field('announcement_text_color', $location->ID);
} else {
	$text_color = $white;
}

if($show): ?>

	<style>
		<?php if($background_color): ?>
			.announcement {
				background-color: <?php echo $background_color; ?> !important;
				color: <?php echo $text_color; ?> !important;
			}
		<?php endif; ?>

		<?php if($text_color): ?>
			.announcement a,
			.announcement p {
				color: <?php echo $text_color; ?> !important;
			}
		<?php endif; ?>
	</style>

	<section class="announcement grid">
		<div class="info">
			<?php echo $announcement; ?>
		</div>
	</section>

<?php endif; ?>
