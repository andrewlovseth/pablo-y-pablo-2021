<nav class="utility-nav grid">
    <div class="links">
        <h5><a href="/">Locations</a></h5>

        <?php
            $front = get_option('page_on_front');
            $locations = get_field('locations', $front); 
            if( $locations ):
        ?>

            <?php foreach( $locations as $location ): ?>

                <a href="<?php echo get_permalink( $location->ID ); ?>" class="<?php echo sanitize_title_with_dashes(get_the_title( $location->ID )); ?>">
                    <?php echo get_the_title( $location->ID ); ?>
                </a>

            <?php endforeach; ?>

        <?php endif; ?>
    </div>    
</nav>