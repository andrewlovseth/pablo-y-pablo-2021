<?php

require_once( plugin_dir_path( __FILE__ ) . '/functions/enqueue-child-styles-scripts.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/acf-pablo.php');

add_filter('body_class', 'bearsmith_body_class');
function bearsmith_body_class( $classes ) {

    global $post;
    $location = bearsmith_get_location($post); 
    $classes[] = $location;
     
    return $classes;    
}