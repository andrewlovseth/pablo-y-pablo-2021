<?php

/*

	Template Name: Home

*/

get_header(); ?>

    <?php get_template_part('templates/home/photos'); ?>

    <?php get_template_part('templates/home/description'); ?>

<?php get_footer(); ?>