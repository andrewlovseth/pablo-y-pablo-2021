<?php

$show = get_field('show_announcement');
$announcement = get_field('announcement');

if(get_field('announcement_background_color')) {
	$background_color = get_field('announcement_background_color');
} else {
	$background_color = $black;
}

if(get_field('announcement_text_color')) {
	$text_color = get_field('announcement_text_color');
} else {
	$text_color = $white;
}

if($show): ?>

	<style>
		<?php if($background_color): ?>
			.announcement {
				background-color: <?php echo $background_color; ?> !important;
				color: <?php echo $text_color; ?> !important;
			}
		<?php endif; ?>

		<?php if($text_color): ?>
			.announcement a,
			.announcement p {
				color: <?php echo $text_color; ?> !important;
			}
		<?php endif; ?>
	</style>

	<section class="announcement grid">
		<div class="info">
			<?php echo $announcement; ?>
		</div>
	</section>

<?php endif; ?>
