<!DOCTYPE html>
<html>
<head>
	<?php echo get_field('head_meta', 'options'); ?>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php echo get_field('body_meta', 'options'); ?>