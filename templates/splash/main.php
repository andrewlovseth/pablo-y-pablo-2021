<?php

    $logo = get_field('logo'); 
    $locations = get_field('locations'); 
    $photo = get_field('photo'); 
    $about = get_field('about');
    $about_photo = $about['photo'];
    $about_copy = $about['copy'];

?>

<section class="splash">
    <div class="splash-header">
        <div class="logo">
            <?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>
        </div>
    </div>

    <div class="splash-body">
        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>

        <div class="links">
            <div class="links-wrapper">

                <?php if( $locations ): ?>
                    <?php foreach( $locations as $location ): ?>

                        <div class="link">
                            <a class="btn" href="<?php echo get_permalink( $location->ID ); ?>">
                                <?php echo get_the_title( $location->ID ); ?>
                            </a>
                        </div>
                        
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>
        </div>
    </div>

    <div class="splash-about grid">
        <div class="photo">
            <?php echo wp_get_attachment_image($about_photo['ID'], 'full'); ?>
        </div>

        <div class="copy">
            <?php echo $about_copy; ?>
        </div>
    </div>
</section>